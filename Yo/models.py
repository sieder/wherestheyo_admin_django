from __future__ import unicode_literals

from django.db import models
from django.contrib import admin
from django.utils.html import format_html, mark_safe
import base64
# import cStringIO
from django.utils.encoding import python_2_unicode_compatible
from django.forms import ModelForm, Textarea
from django import forms

from django.core.files.uploadedfile import SimpleUploadedFile
#esuf = SimpleUploadedFile('uploaded_file.png', image_output.read(), content_type='image/png')
from datetime import datetime
# import js2py

from django.contrib.auth.models import AbstractBaseUser


class ArInternalMetadata(models.Model):
    key = models.CharField(primary_key=True, max_length=255)
    value = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'ar_internal_metadata'

MY_CHOICES = (('private', 'Private'),
          ('public', 'Public'),
          ('restaurant', 'Restaurant'),
          ('userdsa', 'User DSA'))

ADMIN_TYPES = (('Admin', 'Administrator'),
                ('Customer', 'Customer'))

class Areas(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    days_of_availability = models.IntegerField(blank=True, null=True)
    time_schedule = models.IntegerField(blank=True, null=True)
    notes = models.CharField(max_length=255, blank=True, null=True)
    partnership = models.IntegerField(blank=True, null=True)
    promotional_ads = models.CharField(max_length=255, blank=True, null=True)
    poster_ads = models.TextField(blank=True, null=True)
    date_period = models.CharField(max_length=255, blank=True, null=True)
    days = models.CharField(max_length=255, blank=True, null=True, verbose_name='Days Available')
    start_time = models.CharField(max_length=255, blank=True, null=True)
    end_time = models.CharField(max_length=255, blank=True, null=True)
    long = models.DecimalField(max_digits=11, decimal_places=8, blank=True, null=True)
    lat = models.DecimalField(max_digits=11, decimal_places=8, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    typeofdsa = models.CharField(db_column='typeOfDSA', max_length=255, blank=True, null=True, verbose_name='Type of DSA', choices=MY_CHOICES)  # Field name made lowercase.
    verify = models.BooleanField(blank=True, null=True, verbose_name='Verified')
    # poster_ads_image = models.ImageField(null=True, blank=True, upload_to='poster_ads')

    # def image_to_b64(image_file):
    #     with open(image_file.path, "rb") as f:
    #         encoded_string = base64.b64encode(f.read())
    #         return encoded_string

    def image_tag(self):
        imgdata = self.poster_ads
        return format_html('<a href="%s" download alt="No Image" title="Click to Download Image"><img src="%s" width=100 height=100 /></a><br />' % (imgdata, imgdata))

    def operational_hours_tag(self):
        return '%s - %s' % (self.start_time, self.end_time)

    # def upload_image_poster_ads(self):
    #     javas ='''
    #         function testt()
    #         {
    #             alert("test");
    #         }
            
    #         '''
    #     return format_html('<input type="file" id="file-select" onclick="%s"/>' % js2py.eval_js(javas) )

    operational_hours_tag.short_description = 'Operational Hours'
    operational_hours_tag.allow_tags = True
        
    image_tag.short_description = 'Poster Ads'
    image_tag.allow_tags = True

    @python_2_unicode_compatible
    def __str__(self):
        return '%s' % (self.name)
    
    class Meta:
        ordering = ('id',)
        managed = False
        db_table = 'areas'
        verbose_name = 'DSA'
        # proxy = True


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DashboardUserdashboardmodule(models.Model):
    title = models.CharField(max_length=255)
    module = models.CharField(max_length=255)
    app_label = models.CharField(max_length=255, blank=True, null=True)
    user = models.PositiveIntegerField()
    column = models.PositiveIntegerField()
    order = models.IntegerField()
    settings = models.TextField()
    children = models.TextField()
    collapsed = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'dashboard_userdashboardmodule'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'

class Facilities(models.Model):
    id = models.BigAutoField(primary_key=True)
    user_id = models.IntegerField(blank=True, null=True)
    area_id = models.IntegerField(blank=True, null=True)
    well_ventilated = models.BooleanField(blank=True, null=True)
    openarea = models.BooleanField(blank=True, null=True)
    enclosedarea = models.BooleanField(blank=True, null=True)
    wifi = models.BooleanField(blank=True, null=True)
    seats = models.BooleanField(blank=True, null=True)
    ashtrays = models.BooleanField(blank=True, null=True)
    trashbin = models.BooleanField(blank=True, null=True)
    spacious = models.BooleanField(blank=True, null=True)
    roof = models.BooleanField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    area_name = models.CharField(max_length=255, blank=True, null=True)

    @python_2_unicode_compatible
    def __str__(self):
        return '%s' % (self.area_name)    

    class Meta:
        ordering = ('id',)
        managed = False
        verbose_name = 'Facilitie'
        verbose_name_plural = 'Facilities'
        db_table = 'facilities'
        


class JetBookmark(models.Model):
    url = models.CharField(max_length=200)
    title = models.CharField(max_length=255)
    user = models.PositiveIntegerField()
    date_add = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'jet_bookmark'


class JetPinnedapplication(models.Model):
    app_label = models.CharField(max_length=255)
    user = models.PositiveIntegerField()
    date_add = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'jet_pinnedapplication'


class Movies(models.Model):
    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'movies'

class Profiles(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.OneToOneField('Users', on_delete=models.CASCADE)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    birth_month = models.CharField(max_length=255)
    birth_day = models.IntegerField()
    birth_year = models.CharField(max_length=255)
    gender = models.CharField(max_length=255)
    # photo = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'profiles'
        verbose_name = 'Profile'
        


class Ratings(models.Model):
    id = models.BigAutoField(primary_key=True)
    user_id = models.IntegerField(blank=True, null=True)
    area_id = models.IntegerField(blank=True, null=True)
    rating = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'ratings'
        verbose_name = 'Rating'


class SchemaMigrations(models.Model):
    version = models.CharField(primary_key=True, max_length=255)

    class Meta:
        managed = False
        db_table = 'schema_migrations'

#typeofdsa = models.CharField(db_column='typeOfDSA', max_length=255, blank=True, null=True, verbose_name='Type of DSA', choices=MY_CHOICES)  # Field name made lowercase.
# AbstractBaseUser
class Users(models.Model):
# class Users(AbstractBaseUser):
    id = models.BigAutoField(primary_key=True)
    email = models.CharField(max_length=255)
    password_digest = models.CharField(max_length=255, verbose_name='Password')
    account_type = models.CharField(max_length=255, verbose_name='Account Type', choices=ADMIN_TYPES)
    approved = models.BooleanField()
    created_at = models.DateTimeField(editable=False, default=datetime.now)
    updated_at = models.DateTimeField(editable=False, default=datetime.now)

    @python_2_unicode_compatible
    def __str__(self):
        return '%s' % (self.email)

    class Meta:
        managed = False
        db_table = 'users'
        verbose_name = "User"
        ordering = ('id',)

class Feedbacks(models.Model):
    id = models.BigAutoField(primary_key=True)
    user_id = models.BigIntegerField(blank=True, null=True)
    comments = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    satisfaction = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'feedbacks'
        verbose_name = 'Feedback'

class UsersForm(ModelForm):
    
    class Meta:
        model = Users
        fields = ['email', 'account_type']
        # exclude = ['password_digest']
        widgets = {
            'email': Textarea(attrs={'cols': 80, 'rows': 20, 'hidden': 'True' })
        }
