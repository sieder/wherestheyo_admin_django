from django import forms
from .models import Users
from django.contrib import admin
from django.contrib.auth.hashers import BCryptSHA256PasswordHasher

class UsersForm(forms.ModelForm):
    email = forms.EmailField(label="Email", widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Email'}))
    # password_digest = forms.CharField(widget=forms.PasswordInput())
    class Meta:
        model = Users
        fields = ['email', 'account_type', 'password_digest', 'approved']
        widgets = {
            'password_digest': forms.PasswordInput(render_value = True),
        #     # 'email': forms.PasswordInput(render_value = True),
        }

        

   