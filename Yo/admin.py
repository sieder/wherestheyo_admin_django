import csv

from django.contrib import admin
from django import forms
from .forms import UsersForm
from django.contrib import messages
from django.db.models.fields import BLANK_CHOICE_DASH

from django.urls import reverse
from django.shortcuts import redirect

from django.db import models
from .models import Areas, Users, Profiles, Facilities, Feedbacks
from django.utils.html import mark_safe, format_html

from django.http import HttpResponse
from django.contrib.auth.models import User, Group
from django.conf.urls.static import static


# admin.sites.AdminSite.site_header = 'Yo! Admin'
admin.site.unregister(User)
admin.site.unregister(Group)

# logo_url = '/static/images/logo.png'
# admin.site.site_header = mark_safe("<img src='%s' height=50 width=50 />" % logo_url)

admin.site.site_url = None
admin.site.site_title = 'Yo! Admin'

class ProfilesInline(admin.StackedInline):
        model = Profiles


class AreasAdmin(admin.ModelAdmin):

    list_display = [ 'name', 'typeofdsa', 'verify', 'updated_at'] 
    exclude = ['partnership', 'date_period', 'time_schedule', 'days_of_availability', ]
    # 'created_at', 'updated_at', 
    # fields = ['operational_hours_tag']
    # list_display_links = ['name' ]
    # fields = ['image_tag', ]
    # readonly_fields = ['operational_hours_tag', 'image_tag', ]
    list_filter = ['typeofdsa', ]
    search_fields = ['name', ]
    # list_editable = ['created_at', ]
    # prepopulated_fields = {'slug':('name',)}
    list_per_page = 20
    view_on_site = False
    

    # readonly_fields = ('readonly_field',)

    fieldsets = (
        ('Area Info', {
            'fields': ('name', 'typeofdsa', 'address', 'notes', 'days', 'long', 'lat')
        }),
        ('Operational Hours', {
            'fields': ('start_time', 'end_time')
        }),
        ('Poster Image', {
            'fields': ('image_tag', 'poster_ads', 'promotional_ads')
        }),
        ('Creation', {
            'fields': ('created_at', 'updated_at')
        }),
    )

    readonly_fields = ['image_tag']

  

    def export_as_csv(self, request, queryset):
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)

        writer.writerow(field_names)
        for obj in queryset:
            row = writer.writerow([getattr(obj, field) for field in field_names])
        return response
    export_as_csv.short_description = "Export selected"

    actions = ["export_as_csv", ]

    # BLANK_CHOICE_DASH = [("", "---------")]
    
    def get_action_choices(self, request):
        default_choices = [("", "Actions")]
        return super(AreasAdmin, self).get_action_choices(request, default_choices)

    # def headshot_image(self, obj):
    #     return mark_safe('<img src="{url}" width="{width}" height={height} />'.format(
    #         url = obj.headshot.url,
    #         width=obj.headshot.width,
    #         height=obj.headshot.height,
    #         )
    # )
    
    # def get_form(self, request, obj=None, **kwargs):
    #     if obj.type == "1":
    #         self.exclude = ("stock", )
    #     form = super(ProductAdmin, self).get_form(request, obj, **kwargs)
    #     return form

    # def customer_title_fields(self, obj):
    #     return ("%s" % (obj.test_name))
    # customer_title_fields.short_description = 'test name'

admin.site.register(Areas, AreasAdmin)


class UsersAdmin(admin.ModelAdmin):  

    inlines = [
        ProfilesInline,
    ]
    form = UsersForm
    list_display = [ 'email', 'account_type', 'created_at', 'approved']
    search_fields = ['email']
    # list_editable = ['email', 'account_type', 'approved']
    # prepopulated_fields = {'slug':('name',)}
    list_filter = ['account_type', ]
    list_per_page = 20
    change_list_template = 'admin/Yo/user_change_list.html'
    change_form_template = 'admin/Yo/user_change_form.html'

    def export_as_csv(self, request, queryset):
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)

        writer.writerow(field_names)
        for obj in queryset:
            row = writer.writerow([getattr(obj, field) for field in field_names])
        return response
    export_as_csv.short_description = "Export selected"

    actions = ["export_as_csv", ]

    def get_action_choices(self, request):
        default_choices = [("", "Actions")]
        return super(UsersAdmin, self).get_action_choices(request, default_choices)

admin.site.register(Users, UsersAdmin)


class FacilitiesAdmin(admin.ModelAdmin):  

    # form = UsersForm
    list_display = [ 'area_name', 'created_at']
    search_fields = ['satisfaction']
    # # list_editable = ['email', 'account_type', 'approved']
    # # prepopulated_fields = {'slug':('name',)}
    # list_filter = ['account_type', ]
    list_per_page = 20
    # change_list_template = 'admin/Yo/user_change_list.html'
    # change_form_template = 'admin/Yo/user_change_form.html'

    def export_as_csv(self, request, queryset):
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)

        writer.writerow(field_names)
        for obj in queryset:
            row = writer.writerow([getattr(obj, field) for field in field_names])
        return response
    export_as_csv.short_description = "Export selected"

    actions = ["export_as_csv", ]

    def get_action_choices(self, request):
        default_choices = [("", "Actions")]
        return super(FacilitiesAdmin, self).get_action_choices(request, default_choices)

admin.site.register(Facilities, FacilitiesAdmin)

class FeedbacksAdmin(admin.ModelAdmin):  

    # form = UsersForm
    list_display = [ 'id', 'created_at']
    search_fields = ['id']
    # # list_editable = ['email', 'account_type', 'approved']
    # # prepopulated_fields = {'slug':('name',)}
    # list_filter = ['account_type', ]
    list_per_page = 20
    # change_list_template = 'admin/Yo/user_change_list.html'
    # change_form_template = 'admin/Yo/user_change_form.html'

    def export_as_csv(self, request, queryset):
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)

        writer.writerow(field_names)
        for obj in queryset:
            row = writer.writerow([getattr(obj, field) for field in field_names])
        return response
    export_as_csv.short_description = "Export selected"

    actions = ["export_as_csv", ]

    def get_action_choices(self, request):
        default_choices = [("", "Actions")]
        return super(FeedbacksAdmin, self).get_action_choices(request, default_choices)

admin.site.register(Feedbacks, FeedbacksAdmin)


# Register your models here.




logo_url = 'data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAOoAAADmCAYAAADbVhk1AAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAFdRJREFUeNrs3Xl8TFfjBvAneyRBJLZYYiKl0cQyYtfGXsXrRYvaam2HovVSqkVRS0tptPhRU6VVQS21vQ2traENGjFKECVMYoskZBJJZJ35/SG8pRFzbmbu3MTz/aefxpyZc8+dZ8659557rh1kplJragBoBaAhgOcBqADUBOANwB1EypAJ4DaA6wD0AC4AOA3guF6nvSF3ZexkCGY5AK8A6A6gC4A6/A5QKRcPYB+AcAB79TrtvVIZVJVaYwegHYBRAPqwp6Qy3vNuB/ANgAi9TmtSfFBVao0jgCEA3gMQxH1Iz5gYAJ8DWK/XafMVF1SVWmMPYDCAWQD8ub/oGRcH4GMAYXqd1qiIoKrUmuYAVgBoxv1D9IgTAMbqddoomwVVpda4ApgP4D8A7LlPiIpkBLAEwAy9Tpsta1BVak0DAJt5HEokdPzaT6/TxkopbC8hpK8CiGJIiYQEAYhSqTV9pBR2EAzp+wC+BuDMdicS5gzgdU+f4CxDYnSkxYe+hddFF+H+ZRciKrnFAN4397qro5lvypASWdbkwv9OscjQV6XWzAbwIduVyOLaePoEw5AYHVGioKrUmmEAvmB7EllNe0+f4CuGxOg/JR2jqtSalgAOgyeOiKwtF0CIXqc9LhRUlVrjCeAUeKcLkVziATTR67SGov7xSddR/48hJZJVncLcmdejqtSa3rh/2w4Rya+PXqfdUWxQVWqNG4BYALXZXkQ2kQCggV6nzSpu6DuZISWyKV/87xrrP3tUlVpTGcAVAB5sKyKbygDgp9dpU4rqUScypESK4FGYx0d7VJVa4w7gGgBPthGRIhgA1NLrtJl/71EHMKREiuJZmMtHhr5vsl2IFOdhLu1Uao0/gEtsEyJFek6v08bZ4/66u0SkTH0eDH27sy2IFKv7g6FvNgAXtgeRIuUAqGjPkBIpmguAYK7HS6R8TRlUIuULYlCJlM+fQSVSPl8GlUj5vBlUIgaViCyBQSViUImIQSViUImIQSUiBpWIQSUiBpWIGFQiBpWIGFQiBpWIGFQiYlCJGFQiYlCJiEElYlCJiEElIgaViEElIgaViEElIsVwZBM8w7/S9naoVsUTNX28UMnTA64uznBxcQIAZGZlIycnD7dT7yLhWgpSDRlssGc5qDV9vNHv322Ey+3c+weuxN+Sta5D+rVDZa8KZr/+SsIt7Nzzh2J2tl+damgVXB/qRnXRJMgPfr5V4ejoYFbZrKwcXLx8EydPx+Hk6cs4euICbt+5Wyq+5M3V9dC2RYCkst+E7cfdjHsM6s1bdxDS5gWoG9YVKteqWX0M1ITCZDLJUs+Q1oGY++EgoTKDRocqIpyv9miFrh3VqFfXR/L7uLm5oHGQCo2DVBgxqBOMRhNOnLqE8P3R2LXnD6SmZSo2qA3q1cSE0f+SVHb1+n3sUQHAaDRh+vww7N4wHQ725h8ytwyujz49WuLH/x6zeh1dnJ0wd5pYSHeEH8fRqAs2aVM7Ozu0bxuI4QM7IqR1oNWGzS2a1kOLpvXw4X9ew849f2BN2AFcuHRdcUE1pEv7ESkwGpGRma2MwxQlVOL8X9fw3aZDwuWmT+yLihXcrF6/8W92h2/Nyma/Pv1uFuaFbrFJWzZr4o8ta6ZgzdJ3rBbSon7I+vdqiz0/fIQl80aipo+3ooKalp4lqVy6xHJlNqgAELpyFxKTDEJlvCqVx9R3X7VqveqqqmH0sJeFyixcul3247eKFdywZN5IbFnzPoIb+9usJ+/dvSUObp+DcaO6CY2QlNijGhQ0nFdMUDMzszFn8Q/C5Qb0eVH4+FbEnA8GwcnJ/COEP2P02LT9iKxtF9I6ED9vmYXe3VsqYl86Ozti8rje2Prt+6hTu2qp7VHT7rJHLdKe/Sfx6+8xwr/i86cPhoOD5TelV7cWQmcLC4xGTP9kPYxGk2xt9tYbXbB22TuoVsVTcceGTYL8sPP7D9GmeYBtg5rGHtXiZi3YiJzcPKEyDerXwrDXO1i0HuU9ymHGpH5CZdZtOoSzsVdlaScHe3t8OmMIpk3sC3t7OyhVxQpuWLdiAl7v86LtgiqxZ2RQi5FwPQXLvw4XLjfp7X+jelXL9SpTxvdGZW/zr5neSjYgdOUu2Y4FF88ZjgGvvoTSwMHBHgs+egMDbVRfo9GEdAlhTefQt3ir1v2COH2iUBl3d1fMnPy6RT6/UaAKQ/q1EzuWXbxZtlP50yb2VczxqIh50wbj5Q5NbPLZBgnHqUrqURU5hTAvLx8ffboBG1ZNEirXrXNTtG8bJHyc+/iQct60QbCzM384GRF5FuH7omVpm17dWuDNIZ0t8l7ZOXk4eToOp85cgT4hCUkpaQ8PO9zKuaKWjxfqqqqjaeO6CHzet8RDbHt7OyyZOxK9h36Ki5dvyjv8Tc8EBC6xAVDUJA7FzvU9GnUBO8KPC/ccH38wEF37zkZ2Tp6kzx3crx0aNqhj9utzcvMwa8FGWdqkvn8NLJg5tMTvExkVi7Cth3Hw8Gmz28nbqzx6dA7GsAEdUVdVTfJnu7m5YOXiMeg5aD7uZefKF9S0LAnh5tDXvKFS6Bbh4wTfmpUxblR3SZ9XtXJFTBnfW6jM8tXhiL+WbP0dZW+HBTPfgGvhpHkpdGcuo8/QBRg8egnC90UL/ZjdvnMX6zb/ii59Z2HCtNVIvJUquR7+quqYOKanzEPfTAlB5ckks78cC5duFy43etjLkn71Z7zXDx7urma//rL+FrTrfpGlLd7o317y9eL8/AIs+PJH9B3xGU7FXCnxiZlde6PQue9s7Ag/Lvl9Rg7pjKAAXxmHvqX7GFXx96Nu2n5E+Mvl5OQoPIH+xVYN0LNrc6EyMxdsQG5uvtXbwM3NBRNGS+uBMjKzMeLdZVj13c8Wvb6bmZmNiTPWYOHSHyWfC5jyTm/ZvkdSQsehr+Av+PT5YSgwGoXKtWkeYPbxrbOzI+Z+IBbsnXv+wO9/xMrSBsNe74BKFd0lnSwa8c4y/HbsvNXq9tW3P2O+xHnNIa0DrTqr7JHQSbjUYuDQV8y5C1clTdqfMakfynuUe+rr3h7xClS+5k91u5txT7ZJ946ODpLP8r4/+zucOHXJ6nVcvX4/Nmw7LKnsmOFd5Qkqe1R5SJm07+1V/qknh+rUroqxI7oJve+i5TuQcjtdlu3u+FJDeFUqL1xuy65I7P45Srb9M2fRZly6In7JpVNII3h7lbd6/UR7x6ysHOTnFzCoUo6JpEzaH9KvHRoFqp747/M+HARnZ/OvUp0+q0fY1gjZtrtvT/HVL9LvZuHTL7bJun/uX6baJH6s6mCP3t2sP3lD9BhVSRPyS1VQgRJO2i/ilqseXZrhxVYNhI+X5Zp07+TkiJdavyBcbs2GAzZZ4ygyKlbSzfIdQxoqrke9k5rBoJaElEn7QQG+GNL/0SmBHu6umDmlv9D7rPvhEGJiE2Tb1uDGdYWvmxYUGPH95gib7Z+1Gw8Il2nW+Dm4ODtZtV6iN4Gn3c1kUEtC6qT9yeN6o2rlig///72xvR75/6dJSknD5yt2yrqtLZrWFy4TERmDO6m2W3Ts0G8xwidhnJ0d0aShn7KGvmkc+paYlEn7Hu6umPFev4c97FDB2+LmLNos+/o5z/vXEC5z8MgZm+6b/PwCHDl2TrhcfQnbKiI7J09oJKakWUmlNqgPJu2L6tm1OUJaB2L+9MFCE8x/O3YeP+07Ift2SpldFSXD5Zin1kEnXoe6dapZvV4iPb0hnT2qRTyYtC/cG4e+XexZ4Mfl5uZjhoQfBUuoXUPsbg+j0YTL+ls23zcXL98QLuNbq7LV6yUy/GWPakHzl2wVnrQvenJmxdo9iL+aZJPtcxeYd/zgOFoJ1/5uJIpP2Hd3c7V6vUS+K+xRLSjldjoWLd9htffXJyRh5dq9Ntk2c2ZU/bPHUMYlBSn1qFDeTYZ6CfSoaexRLWrDtsP4M0Zvlfee8ak8k+6LInLj+gMmkzL2iZR6yLHuU6rQ0Jc9qkUZjSZM/2S98KT9p9m1Nwq/Hz9vs+3KyxP/gRCZYWVNUuqRI/FGfxEi4UtN44QHizsbexXrJEzaf5KMzGzM+3yzTbfpXnau8HN1vDw9FLE/pNQjMyvH+kEV6FENHPpaR+jKXbiVbLDIey1ath3JMk26L47oNLZKnh6Sjm0trU7tKuLbarD+JA2R+buc62slGZnZmLO45L3gmXPxWL81QhHbdCVB/FJLYEBtm9c78HnxlRuuxFv/zLq5vWR+fgGyZOjhn8mgAkD4vmhERJ4t4fFumKwr3RfnsoTnv7YKft7m9W7RtJ4s22qtY1SDAh8hWaaCCkibtP/A+q0ROHMuXjHbEnNe/AaAbp2a2rTO3l7l0TJYfI7yWRludjD3DhqlDXvLZFDjryVj+WrxSfvJt9Ox2IrXZKWIlLDUS/3naggtd2ppfbq3FL7UknI7HX/F3bB63cw9maTEHtURZdDBI2fw3thewsNmJTwC/u/i9IlIvJWK6tUqCZUbPfxljJ/6tez1dXJyxKjB4svGyLX2VPLtdKzd8PTb8C5dSWRQlcpo4euwlvLT/mjhL3+PLs2wJuwATp6+LGtd3xzSWfhHBQDC98vzlIGsezkWOeHIoS/9w9ZdRyWV+/Sjki3WLcpfVR3j3xRf+DzVkGHzW/MYVCqx2IvXoDsj3jPW96+BedMGy1JHD3dXLF/4FtzKuYj/EO0+qqhFxBhUkuzLVf+VVO61nq0x9d1XrVq3cq7O0IaORUC9WsJlc3LzsPr7fdzBDGrZEBF5VvKjKMYM74p50wbD0dHB4vXy9iqP9V9NROvm0q7dhm09jKSUNJu2rYe7q9XXa7IEnkwqJT7+7Ads+3aqpLtMBvcNQcMGvpj00VrhJWyepH3bIHw2exiqCDzs+e+SUtIkjxRKws3NBf17tUWPLs3Q6IU6D28gSDVk4GjUBWzcfsSqTxZgUMu4UzFXsO6HQxg+sKOk8o0CVdi7eSa+3/wrtN/vk/w0toYv1MEEzb/QKaRRibZn1oKNsj/RO7ixP5Yv1BT5ZPpKnh7o3iUY3bsE48Dh05g861s+yJikWbR8B9q0CJC8EJijowNGDOqEN/q3x4EjpxG+LxqRUReeuuq/X51qCGn9Anp2bY7gxv4l3o5tu49i70GdrG3XJMgP67+aaNaZ8E4hjbBm6XgM0oRKfs4ug/oMy7qXA82kldi1/sMSrYjg6OiArh3U6NpBDQC4duM24q8mITHJgMx7OXB0sIdbORfUquENf1V1VLLg7XNnzsdj+vwweb/kjg4InTtC6HKVumFdjBn+Cr5YtZtBJXHxV5MwbqoWa5e+Y7ETRLVqeKNWDW+r1z0xyYDRk1ZKnostVaeXGsFPwiqHwwd2xPJvwhVx+YhnfUuh346dx9tTVpWq64+JSQYMeOtz3CzBk8qlatsyQFK5ihXcbDpvmkEtA/ZH/ImRE5Yrbn5yUS5evon+oxbZbDXHqlUqSi5bvZong0olc+ToOfQZugAJ11MUW8eIyLN4bfhCXLVhHXNypC9Qp5QbyBnUUi5On4geA+Zi887fFVWv3Nx8zA/dgpHvLrN5r3/+r6uSy8px+x2D+ozIyMzG1I/XYeS7yxB/Ldnm9TkadQHdB8zF6vX7FbFaxt6DOkmrVJ44dckmx9QMahl36LcYdHltNmZ/tgm378j/RLfYi9cwasJyDBodarEZUJagT0hC2NbDQmWMRhMWfPmjYraBl2fKmLy8fHy36RA2/ngEPbs2x/CBHREU4Gu1zzOZTDhw+DS+3XgQkVEXhJc4lcu8z7fgOb/qaNM8wKxtmrlgA6L/jGNQyfrHiNt2H8W23UfxnJ8PunVuipc7NEHg87UlrcL/+Hsfj/4LPx/S4ZdDpxSxtKo5P2DDxi3Fe2N7YcSgjk+ciB9/LRmzF24SfrK9tdmp1BpTWfuSOjk5oprgKfmMzGxFrpVjaR7urlA3qosmQX7w862K2jWroHZNb3i4uT7yUCqTyYSMzGykpmXi6vUUXL2WjIuXb+Lk6cs4e+GqpJX8laKKdwV07aiGumFdVPGugJzcPFy/eQdHjp3Dr7/HoKBAeat9lMmgknRu5VyQX1Bgs2fuEIe+ZIasezlsBAXiWV8iBpWIGFQiBpWIGFQiYlCJGFQiYlCJiEElYlCJiEElYlCJiEElIgaViEElIgaViBhUIgaViBhUImJQiRhUImJQiRhUImJQiYhBJWJQiYhBJaLH2PzZM64mI8qb8rgnyGzJ9i5FfJFNqGTMLbZcir0LHn8imgNM8Cqm3B17ZxTAjkFtlX8H4+5d4bePzNavQvN//K1WwT0syjxbbLlh5Zsiy87hkb95GfOwIuP0E8uM9WhU5A8Dh75ExKASMahExKASMahExKASEYNKxKASEYNKRAwqEYNKRAwqETGoRAwqETGoRAwqETGoRCTE5is85MPuH3fdE4ky2j39e2R6wt+KK2dSwDIsAGCnUmtM3M1EHPoSEYNKxKASEYNKRAwqEYNKRAwqETGoRAwqETGoRAwqETGoRMSgEjGoRMSgEhGDSsSgEhGDSsSgEhGDSkQMKhGDSkQMKhExqEQMKhExqETEoBIxqETEoBIxqESkOLcZVCIGlYgsIIFBJVK+OAaVSPliGFQi5TtpDyCH7UCkWDkAou0BRLItiBQrUq/T5tgDCGdbECnWT8D9CQ/b2RZEirUDAOz1Om0cgGNsDyLFOVaYz4dTCFezTYgU52EuHwR1EwAD24VIMVILc/m/oOp12kwAK9g2RIqxojCXj/SoALAEQAbbh8jm7gL44u9/eBhUvU6bAmAR24jI5hYX5hFF9agAsBhAAtuJyGYSCnOIJwZVr9NmAZjAtiKymQmFOXyEw+N/MCRGx3r6BNcH0JBtRiSrDXqd9pOi/uFJd8+MAxDPdiOSTXxh7mB2UPU6rQHA6+CdNURyyAHQvzB3RXJ40j8YEqOve/oEJwDow3YksqpRep222JtjHIr7R0Ni9J+ePsEA0J5tSWQVs/U67dKnvcjhaS8wJEZHePoEuwNoyzYlsqjP9DrtDHNeaO5SLFNRxLUdIpJsMYAPzH2xncg7q9SaKQA+YxsTlcj7ep1WaBagnegnqNSaPgDWAfBgexMJyQAwVK/TCi/WYCfl01RqTQCALQCC2PZEZokB0E+v08ZKKewgpZAhMTrF0yd4DYByAFpJDTzRM8AIIBTAAL1Oe0vqm5Q4YCq1phmAlQCacZ8QPeIEgDF6nTa6pG9U4gW49TrtCQAtAAwFEMd9Q4S4wjy0sERILdKjPta7OgIYDGAyj1/pGT0OXQwgTK/T5lvyja1ybKlSa+wAtAMwCvenILpzH1IZlYn7S+6uBnBYr9OarPEhVj8JpFJrygF4BUB3AF0A1OG+pVJOD2AfgD0A9up12nvW/kDZz9aq1JoaAFri/v2uAQBUAGoAqMyelxTWUyYDuFkYzFgAZwAc1+u0N+SuzP8PAAJYPlz+WS+LAAAAAElFTkSuQmCC'
admin.site.site_header = mark_safe("<img src='%s' height=50 width=50 />" % logo_url)