from django.http import HttpResponse



def home(request):
    print(request)
    print(dir(request))
    print(request.method)
    print(request.is_ajax)
    print(request.get_full_path())
    print(request.body)
    print(type(request.user))
    if str(request.user) == 'admin':
        print('admin logged in')
    elif str(request.user) != 'admin':
        print('logged user is not admin')
    return HttpResponse("<!DOCTYPE><html><head><title>Yo Admin</title></head><body>Hello World</body></html>")